package com.template.projectfortest

import com.template.projectfortest.base.BaseApplication
import com.template.projectfortest.language.Language
import com.template.projectfortest.util.SharedPref
import com.template.projectfortest.util.Singleton

class MainApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        Singleton.context = applicationContext
        Language.language = SharedPref.getDefaultLanguage(applicationContext) ?: Language.EN
    }

    override fun onTerminate() {
        super.onTerminate()
    }
}