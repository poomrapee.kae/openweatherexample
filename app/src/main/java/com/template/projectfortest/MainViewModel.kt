package com.template.projectfortest

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.survivingwithandroid.weather.lib.WeatherClient
import com.survivingwithandroid.weather.lib.WeatherConfig
import com.survivingwithandroid.weather.lib.client.okhttp.WeatherDefaultClient
import com.survivingwithandroid.weather.lib.exception.WeatherLibException
import com.survivingwithandroid.weather.lib.model.City
import com.survivingwithandroid.weather.lib.model.CurrentWeather
import com.survivingwithandroid.weather.lib.provider.openweathermap.OpenweathermapProviderType
import com.survivingwithandroid.weather.lib.request.WeatherRequest
import com.template.projectfortest.base.BaseViewModel
import com.template.projectfortest.network.ApiService
import com.template.projectfortest.network.ServiceEndpointInterface
import com.template.projectfortest.repository.ServiceRepository
import com.template.projectfortest.repository.ServiceRepositoryInterface

class MainViewModel : BaseViewModel() {

    var currentWeather: MutableLiveData<CurrentWeather> = MutableLiveData<CurrentWeather>()

    fun callServiceGetService(mService: ServiceEndpointInterface, jsonObject: JsonObject) {
        ServiceRepository(mService).getService(jsonObject, object : ServiceRepositoryInterface.ServiceCallback {
            override fun onSuccess(jsonObject: JsonObject) {
//                Handle Success
            }

            override fun onFail() {
//                Handle Fail
            }
        })
    }

    fun getWeather(context: Context, cityName: String) {
        ApiService.getWeatherClient(context).searchCity(cityName, object : WeatherClient.CityEventListener {
            override fun onCityListRetrieved(cityList: MutableList<City>?) {
                if (cityList != null) {
                    if (cityList.size != 0)
                        currentWeatheror(context, cityList[0])
                }

            }

            override fun onConnectionError(t: Throwable?) {

            }

            override fun onWeatherError(wle: WeatherLibException?) {

            }
        })


//
    }

    private fun currentWeatheror(context: Context, city: City) {
        ApiService.getWeatherClient(context).getCurrentCondition(WeatherRequest(city.id), object : WeatherClient.WeatherEventListener {
            override fun onWeatherRetrieved(weather: CurrentWeather?) {
                currentWeather.value = weather
            }

            override fun onConnectionError(t: Throwable?) {
            }

            override fun onWeatherError(wle: WeatherLibException?) {
            }
        })

    }
}