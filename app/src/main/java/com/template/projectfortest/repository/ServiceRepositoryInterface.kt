package com.template.projectfortest.repository

import com.google.gson.JsonObject

interface ServiceRepositoryInterface {

    interface ServiceCallback {
        fun onSuccess(jsonObject: JsonObject)

        fun onFail()
    }

    fun getService(request: JsonObject, callback: ServiceCallback)
}