package com.template.projectfortest.repository

import com.google.gson.JsonObject
import com.template.projectfortest.network.ServiceEndpointInterface
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ServiceRepository(private val mService: ServiceEndpointInterface) : ServiceRepositoryInterface {

    override fun getService(request: JsonObject, callback: ServiceRepositoryInterface.ServiceCallback) {
        mService.getService(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: Observer<JsonObject?> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: JsonObject) {
                    callback.onSuccess(t)
                }

                override fun onError(e: Throwable) {
                    callback.onFail()
                }
            })
    }
}