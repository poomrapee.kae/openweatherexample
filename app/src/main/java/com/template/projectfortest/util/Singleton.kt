package com.template.projectfortest.util

import android.annotation.SuppressLint
import android.content.Context

@SuppressLint("StaticFieldLeak")
object Singleton {

    lateinit var context: Context
}