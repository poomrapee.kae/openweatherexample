package com.template.projectfortest.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.template.projectfortest.network.ErrorModel

open class BaseViewModel : ViewModel() {

    val errorModel: MutableLiveData<ErrorModel> = MutableLiveData()

    inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)
}