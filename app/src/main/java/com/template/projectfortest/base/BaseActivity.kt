package com.template.projectfortest.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders

abstract class BaseActivity<B : ViewDataBinding, VM : ViewModel> : AppCompatActivity(), View.OnClickListener {

    abstract var layoutResource: Int
    abstract var viewModelClass: Class<VM>
    lateinit var binding: B
    private var mLastClickTime: Long = 0
    lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutResource)
        viewModel = ViewModelProviders.of(this).get(viewModelClass)
    }


}