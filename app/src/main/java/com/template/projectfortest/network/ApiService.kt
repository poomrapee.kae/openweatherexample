package com.template.projectfortest.network

import android.content.Context
import com.google.gson.GsonBuilder
import com.survivingwithandroid.weather.lib.WeatherClient
import com.survivingwithandroid.weather.lib.WeatherConfig
import com.survivingwithandroid.weather.lib.client.okhttp.WeatherDefaultClient
import com.survivingwithandroid.weather.lib.exception.WeatherProviderInstantiationException
import com.survivingwithandroid.weather.lib.provider.openweathermap.OpenweathermapProviderType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiService {

    private val gson = GsonBuilder()
//                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        .setLenient()
        .setPrettyPrinting()
        .create()

    private fun getLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    private fun getOkHttpClient(context: Context): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(ServiceProperties.TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(ServiceProperties.TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(ServiceProperties.TIMEOUT, TimeUnit.SECONDS)
            .addNetworkInterceptor(getLoggingInterceptor())
            .addInterceptor(getLoggingInterceptor())
//            .addInterceptor(Interceptor { chain ->
//                var request = chain?.request()?.newBuilder()
//                if (!SharedPref.getAccessToken(context).isNullOrEmpty()) {
//                    request?.addHeader("Authorization", SharedPref.getAccessToken(context)!!)
//                }
//                request?.addHeader("Accept-Language", Word.getLang())
//                return@Interceptor chain?.proceed(request?.build())!!
//            })
        return okHttpClient.build()
    }

    private fun getRetrofit(context: Context): Retrofit {
        return Retrofit.Builder()
            .client(getOkHttpClient(context))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(ServiceProperties.getBaseUrl())
            .build()
    }

    fun getEndpointInterface(context: Context): ServiceEndpointInterface {
        return getRetrofit(context).create(ServiceEndpointInterface::class.java)
    }

    fun getWeatherClient(context: Context): WeatherClient {

        val weatherConfig = WeatherConfig()
        weatherConfig.ApiKey = "191b378b2f61e74bd42ceeaaaf5ffc9d"
        weatherConfig.unitSystem = WeatherConfig.UNIT_SYSTEM.M


        var weatherClient = WeatherClient.ClientBuilder()
                .attach(context)
                .httpClient(WeatherDefaultClient::class.java)
                .provider(OpenweathermapProviderType())
                .config(weatherConfig)
                .build()


        weatherClient.updateWeatherConfig(weatherConfig)

        return weatherClient
    }
}