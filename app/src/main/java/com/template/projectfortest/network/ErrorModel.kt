package com.template.projectfortest.network

class ErrorModel {

    var code: String? = ""
    var message: String? = ""
    var description: String? = ""
}