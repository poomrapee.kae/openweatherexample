package com.template.projectfortest.network

import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface ServiceEndpointInterface {

    @POST("")
    fun getService(@Body request: JsonObject): Observable<JsonObject>
}