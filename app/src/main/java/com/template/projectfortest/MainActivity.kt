package com.template.projectfortest

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import androidx.lifecycle.Observer
import com.google.gson.JsonObject
import com.survivingwithandroid.weather.lib.WeatherClient
import com.template.projectfortest.base.BaseActivity
import com.template.projectfortest.databinding.ActivityMainBinding
import com.template.projectfortest.network.ApiService

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {


    override var layoutResource: Int = R.layout.activity_main
    override var viewModelClass = MainViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        viewModel.callServiceGetService(ApiService.getEndpointInterface(this), JsonObject())
        initInstant()
        subscribeData()
    }

    private fun subscribeData() {
        viewModel.currentWeather.observe(this, Observer {
            binding.tvHumidity.text = it.weather.currentCondition.humidity.toString()
            if (binding.radioGroup.checkedRadioButtonId == binding.rdCelsius.id) {
                binding.tvTemperature.text = it.weather.temperature.temp.toString()
            } else {
                var f = (it.weather.temperature.temp * 1.8) + 32
                binding.tvTemperature.text = String.format("%.1f", f)

            }
        })

    }

    private fun initInstant() {
        binding.radioGroup.check(binding.rdCelsius.id)
        binding.radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                var currentWeatherModel = viewModel.currentWeather.value
                if (currentWeatherModel != null)
                    if (binding.radioGroup.checkedRadioButtonId == binding.rdCelsius.id) {
                        binding.tvTemperature.text = currentWeatherModel.weather.temperature.temp.toString()
                    } else {
                        var f = (currentWeatherModel.weather.temperature.temp * 1.8) + 32
                        binding.tvTemperature.text = String.format("%.2f", f)
                    }
            }

        })
        binding.btnOk.setOnClickListener(this)


    }

    override fun onClick(view: View?) {
        when (view) {
            binding.btnOk -> {
                if (!binding.edtCityName.text.toString().isNullOrEmpty()) {
                    viewModel.getWeather(this, binding.edtCityName.text.toString())
                }
            }
        }
    }
}
