package com.template.projectfortest.template

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.template.projectfortest.R
import com.template.projectfortest.base.BaseActivity
import com.template.projectfortest.databinding.ActivityTemplateBinding

class TemplateActivity : BaseActivity<ActivityTemplateBinding, TemplateViewModel>() {
    override fun onClick(p0: View?) {
    }

    override var layoutResource: Int = R.layout.activity_template
    override var viewModelClass = TemplateViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initInstance()
        subscribeData()
    }

    private fun initInstance() {

    }

    private fun subscribeData() {
        viewModel.errorModel.observe(this, Observer {

        })
    }
}