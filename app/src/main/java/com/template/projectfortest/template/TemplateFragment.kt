package com.template.projectfortest.template

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.template.projectfortest.R
import com.template.projectfortest.base.BaseFragment
import com.template.projectfortest.databinding.FragmentTemplateBinding

class TemplateFragment : BaseFragment<FragmentTemplateBinding, TemplateFragmentViewModel>() {

    override var layoutResource: Int = R.layout.fragment_template
    override var viewModelClass = TemplateFragmentViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstance()
        subscribeData()
    }

    private fun initInstance() {

    }

    private fun subscribeData() {
        viewModel.errorModel.observe(this, Observer {

        })
    }
}